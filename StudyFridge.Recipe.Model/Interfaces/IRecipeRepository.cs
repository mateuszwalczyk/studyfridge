﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace StudyFridge.Recipe.Model.Interfaces
{
    public interface IRecipeRepository
    {
        void AddRecipe(RecipeDetail recipeDetails);

        IEnumerable<Recipe> GetRecipes(string recipeName);

        RecipeDetail GetRecipeDetails(int recipeId);
        void RateRecipe(RateRecipe rateRecipe);
        IEnumerable<Recipe> SearchRecipe(string ingredientName);
    }
}
