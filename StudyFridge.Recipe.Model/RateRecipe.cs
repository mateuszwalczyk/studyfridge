﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StudyFridge.Recipe.Model
{
    public class RateRecipe : Recipe
    {
        public int RateRecipeId { get; set; }
        public int RateRecipeValue { get; set; }
    }
}
