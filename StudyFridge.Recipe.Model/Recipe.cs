﻿using System;

namespace StudyFridge.Recipe.Model
{
    public class Recipe
    {
        public int RecipeId { get; set; }
        public string Name { get; set; }
        public decimal StarRating { get; set; }
    }
}
