﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StudyFridge.Recipe.Model
{
    public class RecipeDetail : Recipe
    {
        public int RecipeDetailId { get; set; }
        public IEnumerable<Ingredient> Ingredients { get; set; }
        public string Description { get; set; }
    }
}
