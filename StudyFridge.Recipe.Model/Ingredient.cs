﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StudyFridge.Recipe.Model
{
    public class Ingredient 
    {
        public int IngredientId { get; set; }
        public int RecipeId { get; set; }
        public string Name { get; set; }
        public string Quantity { get; set; }
    }
}
