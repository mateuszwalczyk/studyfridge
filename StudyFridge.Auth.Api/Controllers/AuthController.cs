﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using StudyFridge.Auth.Core;
using StudyFridge.Auth.Model;
using StudyFridge.Auth.Model.Interfaces;

namespace StudyFridge.Auth.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    public class AuthController : ControllerBase
    {
        private IAuthService _authService;
        private readonly ILogger<AuthController> _logger;

        public AuthController(ILogger<AuthController> logger, IAuthService authService)
        {
            _logger = logger;
            _authService = authService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok( _authService.GenerateSecurityToken("mateuszwalczyk@gmail.com"));
        }

        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody] User user)
        {
            var token = _authService.AuthenticateUser(user);

            return Ok(token);
        }

        [HttpPost("adduser")]
        public void AddUser([FromBody] User user)
        {
            _authService.AddUser(user);
        }
        
        [HttpGet("getusers")]
        public IActionResult GetUsers()
        {
            return Ok(_authService.GetUsers());
        }
    }
}