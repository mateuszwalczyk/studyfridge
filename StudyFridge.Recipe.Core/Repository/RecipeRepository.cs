﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using StudyFridge.Recipe.Core.Db;
using StudyFridge.Recipe.Core.Db.Entities;
using StudyFridge.Recipe.Model;
using StudyFridge.Recipe.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace StudyFridge.Recipe.Core.Repository
{
    public class RecipeRepository : IRecipeRepository
    {
        private readonly IConfiguration _configuration;

        public RecipeRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public void AddRecipe(RecipeDetail recipeDetails)
        {
            using (var db = new RecipeDbContext(_configuration))
            {
                var newRecipe = new RecipeEntity
                {
                    Name = recipeDetails.Name,
                    StarRating = recipeDetails.StarRating
                };
                db.Recipes.Add(newRecipe);
                db.SaveChanges();
                var recipeDetail = new Db.Entities.RecipeDetailEntity
                {
                    RecipeId = newRecipe.RecipeId,
                    Description = recipeDetails.Description,
                };
                db.RecipeDetails.Add(recipeDetail);



                foreach(var item in recipeDetails.Ingredients)
                {
                    db.Ingredients.Add(new IngredientEntity
                    {
                        Name = item.Name,
                        Quantity = item.Quantity,
                        RecipeId = newRecipe.RecipeId
                    });
                }
                db.SaveChanges();
            }
        }

        public IEnumerable<Model.Recipe> GetRecipes(string recipeName)
        {
            using (var db = new RecipeDbContext(_configuration))
            {
                var result = db.Recipes.AsQueryable();
                
                if(!string.IsNullOrWhiteSpace(recipeName))
                {
                    result = result.Where(x => x.Name.Contains(recipeName));
                }

                return result.OrderBy(x => x.Name).Select(x => new Model.Recipe
                {
                    RecipeId = x.RecipeId,
                    Name = x.Name,
                    StarRating = x.StarRating,
                }).ToList();
            }
        }

        public RecipeDetail GetRecipeDetails(int recipeId)
        {
            using(var db = new RecipeDbContext(_configuration))
            {
                var recipe = db.Recipes.FirstOrDefault(x => x.RecipeId == recipeId);
                var recipeDetails = db.RecipeDetails.FirstOrDefault(x => x.RecipeId == recipeId);
                var ingredients = db.Ingredients.Where(x => x.RecipeId == recipeId).Select(x => new Ingredient
                {
                    RecipeId = x.RecipeId,
                    IngredientId = x.IngredientId,
                    Name = x.Name,
                    Quantity = x.Quantity
                });
                return new RecipeDetail
                {
                    RecipeId = recipe.RecipeId,
                    Name = recipe.Name,
                    Ingredients = ingredients.ToList(),
                    Description = recipeDetails.Description,
                    StarRating = recipe.StarRating
                };
                   
            }
        }

        public void RateRecipe(RateRecipe rateRecipe)
        {
            using (var db = new RecipeDbContext(_configuration))
            {     
                db.Database.ExecuteSqlCommand("dbo.CountRate @RecipeId={0}, @RateRecipeValue={1}", rateRecipe.RecipeId, rateRecipe.RateRecipeValue);
            }
        }

        public IEnumerable<Model.Recipe> SearchRecipe(string ingredientName)
        {
            using (var db = new RecipeDbContext(_configuration))
            {
                var recipes = db.Recipes.FromSqlRaw("dbo.SearchRecipe @Name={0}", ingredientName).ToList();
                return recipes.Select(item => new Model.Recipe {
                    Name = item.Name,
                    RecipeId = item.RecipeId,
                    StarRating = item.StarRating
                });
            }
        }
    }
}
