﻿using Microsoft.Extensions.Configuration;
using StudyFridge.Recipe.Model;
using StudyFridge.Recipe.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace StudyFridge.Recipe.Core
{
    public class RecipeService : IRecipeService
    {
        private readonly IRecipeRepository _recipeRepository;

        public RecipeService(IConfiguration configuration, IRecipeRepository recipeRepository)
        {
            _recipeRepository = recipeRepository;
        }
        public void AddRecipe(RecipeDetail recipeDetails)
        {
            _recipeRepository.AddRecipe(recipeDetails);
        }

        public RecipeDetail GetRecipeDetails(int recipeId)
        {
            return _recipeRepository.GetRecipeDetails(recipeId);
        }

        public IEnumerable<Model.Recipe> GetRecipes(string recipeName) 
        {
            return _recipeRepository.GetRecipes(recipeName);
        }
        public void RateRecipe(RateRecipe rateRecipe)
        {
            _recipeRepository.RateRecipe(rateRecipe);
        }
        public IEnumerable<Model.Recipe> SearchRecipe(string ingredienName)
        {
            return _recipeRepository.SearchRecipe(ingredienName);
        }
    }
}
