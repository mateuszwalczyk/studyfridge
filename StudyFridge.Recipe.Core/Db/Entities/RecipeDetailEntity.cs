﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace StudyFridge.Recipe.Core.Db.Entities
{
    [Table("RecipeDetail")]
    public class RecipeDetailEntity
    {
        [Column("RecipeDetailId")]
        [Key]
        public int RecipeDetailId { get; set; }
        [Column("RecipeId")]
        public int RecipeId { get; set; }
        [Column("Description")]
        public string Description { get; set; }

    }
}
