﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace StudyFridge.Recipe.Core.Db.Entities
{
    [Table("Recipe")]
    public class RecipeEntity
    {
        [Column("RecipeId")]
        [Key]
        public int RecipeId { get; set; }

        [Column("Name")]
        public string Name { get; set; }

        [Column("StarRating")]
        public decimal StarRating { get; set; }
    }

}
