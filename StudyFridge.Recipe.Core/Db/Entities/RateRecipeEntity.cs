﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace StudyFridge.Recipe.Core.Db.Entities
{
    [Table("RateRecipe")]
    public class RateRecipeEntity
    {
        [Column("RateRecipeId")]
        [Key]
        public int RateRecipeId { get; set; }
        [Column("RecipeId")]
        public int RecipeId { get; set; }
        [Column("RateRecipeValue")]
        public int RateRecipeValue { get; set; }

    }
}
