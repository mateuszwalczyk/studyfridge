﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace StudyFridge.Recipe.Core.Db.Entities
{
    [Table("Ingredient")]
    public class IngredientEntity
    {
        [Column("IngredientId")]
        [Key]
        public int IngredientId { get; set; }
        [Column("RecipeId")]
        public int RecipeId { get; set; }
        [Column("Name")]
        public string Name { get; set; }
        [Column("Quantity")]
        public string Quantity { get; set; }

    }
}
