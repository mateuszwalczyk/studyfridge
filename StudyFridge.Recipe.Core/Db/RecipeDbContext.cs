﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using StudyFridge.Recipe.Core.Db.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace StudyFridge.Recipe.Core.Db
{
    public class RecipeDbContext : DbContext
    {
        private readonly string _connectionString;

        public RecipeDbContext(IConfiguration configuration)
        {
            _connectionString = configuration.GetSection("ConnectionStrings").GetSection("StudyFridgeDb").Value;
        }

        public DbSet<RateRecipeEntity> RateRecipes { get; set; }
        public DbSet<RecipeEntity> Recipes { get; set; }
        public DbSet<RecipeDetailEntity> RecipeDetails { get; set; }
        public DbSet<IngredientEntity> Ingredients { get; set; }       
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_connectionString);
        }

    }
}
