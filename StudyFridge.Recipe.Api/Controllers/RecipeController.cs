﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using StudyFridge.Recipe.Core;
using StudyFridge.Recipe.Model;
using StudyFridge.Recipe.Model.Interfaces;

namespace StudyFridge.Recipe.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    public class RecipeController : ControllerBase
    {
        private IRecipeService _recipeService;
        private readonly ILogger<RecipeController> _logger;

        public RecipeController(ILogger<RecipeController> logger, IRecipeService recipeService)
        {
            _logger = logger;
            _recipeService = recipeService;
        }

        [HttpPut("addrecipe")]
        public void AddRecipe([FromBody] RecipeDetail recipeDetails)
        {
            _recipeService.AddRecipe(recipeDetails);
        }

        [HttpGet("getrecipes")]
        public IActionResult GetRecipes(string recipeName)
        {
            return Ok(_recipeService.GetRecipes(recipeName));
        }

        [HttpGet("getrecipedetails")]
        public IActionResult GetRecipeDetails(int recipeId)
        {
            return Ok(_recipeService.GetRecipeDetails(recipeId));
        }

        [HttpPut("raterecipe")]
        public void RateRecipe([FromBody] RateRecipe rateRecipe)
        {
            _recipeService.RateRecipe(rateRecipe);
        }

        [HttpGet("searchrecipe")]
        public IActionResult SearchRecipe(string ingredientName)
        {
            return Ok(_recipeService.SearchRecipe(ingredientName));
        }

    }
}
