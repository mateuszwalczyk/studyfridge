﻿CREATE TABLE Ingredient
(
	IngredientId int identity(1,1) not null primary key
	,RecipeId int FOREIGN KEY REFERENCES dbo.Recipe(RecipeId) not null
	,Name nvarchar(255) not null
	,Quantity nvarchar(255) not null
)
