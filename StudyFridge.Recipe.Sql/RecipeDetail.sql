﻿CREATE TABLE RecipeDetail
(
	RecipeDetailId int identity(1,1) not null primary key
	,RecipeId int FOREIGN KEY REFERENCES dbo.Recipe(RecipeId) not null
	,Description nvarchar(max) not null
)