﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StudyFridge.Auth.Model
{
    public class UserDetails : User
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
