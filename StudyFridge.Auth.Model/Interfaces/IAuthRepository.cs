﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace StudyFridge.Auth.Model.Interfaces
{
    public interface IAuthRepository
    {
        bool IsAuthorized(User user);

        void AddUser(User user);

        IEnumerable<User> GetUsers();
    }
}
