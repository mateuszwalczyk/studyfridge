﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using StudyFridge.Auth.Core.Db;
using StudyFridge.Auth.Model;
using StudyFridge.Auth.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;

namespace StudyFridge.Auth.Core.Repository
{
    public class AuthRepository : IAuthRepository
    {
        private readonly IConfiguration _configuration;
        public AuthRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void AddUser(User user)
        {
            using (var db = new AuthDbContext(_configuration))
            {
                var newUser = new StudyFridge.Auth.Core.Db.Entities.UserEntity
                {
                    Email = user.Email,
                    Password = user.Password
                };
                db.Users.Add(newUser);
                db.SaveChanges();
            }
        }

        public IEnumerable<User> GetUsers()
        {
            using (var db = new AuthDbContext(_configuration))
            {

                return db.Users.Select(x => new User
                {
                    UserId = x.UserId,
                    Email = x.Email,
                    Password = x.Password
                }).ToList();
            }
        }

        public bool IsAuthorized(User user)
        {
            using (var db = new AuthDbContext(_configuration))
            {
               return  db.Users.Any(x => x.Email == user.Email && x.Password == user.Password);
            }
        }
    }
}
