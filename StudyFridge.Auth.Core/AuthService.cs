﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using StudyFridge.Auth.Model;
using StudyFridge.Auth.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace StudyFridge.Auth.Core
{
    public class AuthService: IAuthService
    {
        private readonly string _secret;
        private readonly string _expDate;
        private readonly IAuthRepository _authRepository;
        public AuthService(IConfiguration configuration, IAuthRepository authRepository)
        {
            _secret = configuration.GetSection("JwtConfig").GetSection("secret").Value;
            _expDate = configuration.GetSection("JwtConfig").GetSection("expirationInMinutes").Value;
            _authRepository = authRepository;
        }

        public string GenerateSecurityToken(string email)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim(ClaimTypes.Email, email)
                }),
                Expires = DateTime.UtcNow.AddMinutes(double.Parse(_expDate)),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);

        }

        public string AuthenticateUser(User user)
        {
            if(_authRepository.IsAuthorized(user))
            {
                return GenerateSecurityToken(user.Email);
            }

            return null;
        }

        public void AddUser(User user)
        {
            _authRepository.AddUser(user);
        }

        public IEnumerable<User> GetUsers()
        {
            return _authRepository.GetUsers();
        }
    }
}
