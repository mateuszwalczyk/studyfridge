﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace StudyFridge.Auth.Core.Db.Entities
{
    [Table("User")]
    public class UserEntity
    {
        [Column("UserId")]
        [Key]
        public int UserId { get; set; }
        
        [Column("Email")]
        public string Email { get; set; }
       
        [Column("Password")]
        public string Password { get; set; }

    }
}
