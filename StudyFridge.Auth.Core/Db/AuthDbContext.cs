﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;
using Microsoft.Extensions.Configuration;
using StudyFridge.Auth.Core.Db.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace StudyFridge.Auth.Core.Db
{
    public class AuthDbContext : DbContext
    {
        private readonly string _connectionString;
        public AuthDbContext(IConfiguration configuration)
        {
            _connectionString = configuration.GetSection("ConnectionStrings").GetSection("StudyFridgeDb").Value;

        }
        public DbSet<UserEntity> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //var connectionString = @"Data Source=(local);Initial Catalog=StudyFridgeDb;User ID=sa;Password=<ckple17ler>;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
            optionsBuilder.UseSqlServer(_connectionString);
        }
    }
}
